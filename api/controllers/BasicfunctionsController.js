/**
 * BasicfunctionsController
 *
 * @description :: Server-side logic for managing basicfunctions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req, res){
		//res.json(200, { status: 'ok' })
		var url = 'http://www.dneonline.com/calculator.asmx?WSDL';
		
		//var values = req.allParams();
		if (req.param('operation') === 'Factorial' && req.param('num')) {
			resultado = 1;
			for (var i = 1; i <= req.param('num'); i++) {
				resultado *= i;
			}
			res.json(200, { response : resultado })
		} else if (!req.param('operation') || !req.param('intA') || !req.param('intB')) {
			res.json(400, { response : 'faltan parametros' });
		}else {

			var args = {intA: req.param('intA'), intB: req.param('intB')};

			switch(req.param('operation')) {
			    case 'Add':
			    	sails.soap.createClient(url, function(err, client) {
						if (!err) {
							client.Add(args, function(err, result) {
					        	res.json(200, { response : result })
					    	});	
						} else {
							res.json(503, { status : 'servicio remoto no disponible.', error : err} )				    
						}
					})
			        break;
			    case 'Subtract':
			    	sails.soap.createClient(url, function(err, client) {
						if (!err) {
							client.Subtract(args, function(err, result) {
					        	res.json(200, { response : result });
					    	});	
						} else {
							res.json(503, { status : 'servicio remoto no disponible.', error : err} )				    
						}
					});
			        break;
			    case 'Multiply':
			    	sails.soap.createClient(url, function(err, client) {
						if (!err) {
							client.Multiply(args, function(err, result) {
					        	res.json(200, { response : result });
					    	});	
						} else {
							res.json(503, { status : 'servicio remoto no disponible.', error : err} )				    
						}
					});
			        break;
			    case 'Divide':
			    	sails.soap.createClient(url, function(err, client) {
						if (!err) {
							client.Divide(args, function(err, result) {
					        	res.json(200, { response : result });
					    	});	
						} else {
							res.json(503, { status : 'servicio remoto no disponible.', error : err} )				    
						}
					});
			        break;		    	        
			};
		};	


		
	}
};

